const express = require('express')//1
const path = require('path')//2
const PORT = process.env.PORT || 5000
const mongoose = require('mongoose')//3
var createError = require('http-errors');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var cryptosRouter = require('./routes/cryptos')

var app = express();

//Conexion DB
mongoose.connect(process.env.DATABASE_URL)
.then((connection)=>{
  console.log('Conexion a la base de datos satisfactoria', connection)
})
.catch((error)=>{
  console.error('Error al conectar a la base de datos', error)
})
// view engine setup
const rutas1 = require('./routes/')

var app = express();

app.use(express.static(path.join(__dirname, 'public')))      //Path
app.set('views', path.join(__dirname, 'views'))              //Views
app.set('view engine', 'ejs')                                //Engine PUG
app.get('/', (req, res) => res.render('pages/index'))        //Index
app.get('/', rutas1)
app.listen(PORT, () => console.log(`Listening on ${ PORT }`))//Port


//Cosas 1
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

//Cosas 2
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/',cryptosRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;